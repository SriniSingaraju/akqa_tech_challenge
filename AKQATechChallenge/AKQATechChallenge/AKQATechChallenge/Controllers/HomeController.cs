﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AKQATechChallenge.Controllers
{
    /// <summary>
    /// Displays Index page to enter name and number and submit for conversion to words
    /// </summary>
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}