﻿$(".btn-submit").click(function () {
    var nameAndnumber = $("#input-nameandnumber").val();
    var name = nameAndnumber.replace(/[^a-zA-Z\' ']/g, '');
    var decimalNumber = Number(nameAndnumber.replace(/[^0-9\.]+/g, ""));
    var apiUrl = 'http://localhost:59361/api/ConversionToWord/Words/' + decimalNumber + "/";    
    var httpMethod = 'GET';
    $.ajax({
        cache: false,
        type: httpMethod,
        url: apiUrl,
        //contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            number: decimalNumber
        }),
        success: function (response) {
            $("#name").html(name);
            $("#words").html(response);
        }
    });
});
