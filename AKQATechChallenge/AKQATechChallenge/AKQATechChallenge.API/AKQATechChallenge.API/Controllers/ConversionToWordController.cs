﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AKQATechChallenge.Service;
using System.Web.Http.Cors;

namespace AKQATechChallenge.API
{    
    [EnableCors(origins: "http://localhost:49546", headers: "*", methods: "*")]
    public class ConversionToWordController : ApiController
    {
        INumberToWordConverter numberToWordsConverter;

        /// <summary>
        /// API controller constructor
        /// For production code, dependency injection such as AutoFac should be used instead of instantiating the service method in the controller
        /// </summary>
        public ConversionToWordController()
        {
            numberToWordsConverter = new NumberToWordConverter();
        }

        /// <summary>
        /// API Action calls service method to convert the provided number to words
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>        
        [Route("api/ConversionToWord/Words/{number}/")]
        [HttpGet]
        public HttpResponseMessage Get([FromUri]string number)
        {
            try
            {
                string words = numberToWordsConverter.ConvertNumberToWords(number);

                return Request.CreateResponse(HttpStatusCode.OK, words);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Failed to convert the number to words");
            }
        }
    }
}
