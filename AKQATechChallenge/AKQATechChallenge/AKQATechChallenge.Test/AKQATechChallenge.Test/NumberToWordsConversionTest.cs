﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using AKQATechChallenge.Service;

namespace AKQATechChallenge.Test
{
    /// <summary>
    /// Testing negative, zero, integer, double, letters, blank, space, number greater than 100 billion
    /// </summary>
    public class NumberToWordsConversionTest
    {
        NumberToWordConverter numberToWordsConversion = new NumberToWordConverter();

        [Fact]
        public void Number_Hundred_Decimal_TwoDigitPrecision()
        {
            var numberInWords = numberToWordsConversion.ConvertNumberToWords("123.45");

            string expectedWords = "one hundred and twenty-three dollars and forty-five cents".ToUpper();

            Assert.Equal(expectedWords, numberInWords);
        }

        [Fact]
        public void Number_Thousand_Decimal_TwoDigitPrecision()
        {
            var numberInWords = numberToWordsConversion.ConvertNumberToWords("1234.56");

            string expectedWords = "one thousand two hundred and thirty-four dollars and fifty-six cents".ToUpper();

            Assert.Equal(expectedWords, numberInWords);
        }

        [Fact]
        public void Number_Thousand_Decimal_FourDigitPrecision()
        {
            var numberInWords = numberToWordsConversion.ConvertNumberToWords("1234.5645");

            string expectedWords = "one thousand two hundred and thirty-four dollars and fifty-six cents".ToUpper();

            Assert.Equal(expectedWords, numberInWords);
        }

        [Fact]
        public void Number_Thousand_Decimal_FourDigitPrecision_Rounded()
        {
            var numberInWords = numberToWordsConversion.ConvertNumberToWords("1234.5678");

            string expectedWords = "one thousand two hundred and thirty-four dollars and fifty-seven cents".ToUpper();

            Assert.Equal(expectedWords, numberInWords);
        }

        [Fact]
        public void Number_Thousand_Integer()
        {
            var numberInWords = numberToWordsConversion.ConvertNumberToWords("12345");

            string expectedWords = "twelve thousand three hundred and forty-five dollars".ToUpper();

            Assert.Equal(expectedWords, numberInWords);
        }

        [Fact]
        public void Number_Thousand_TwoDigitPrecision_Negative()
        {
            var numberInWords = numberToWordsConversion.ConvertNumberToWords("-1234.56");

            string expectedWords = "minus one thousand two hundred and thirty-four dollars and fifty-six cents".ToUpper();

            Assert.Equal(expectedWords, numberInWords);
        }

        [Fact]
        public void Number_Million_TwoDigitPrecision()
        {
            var numberInWords = numberToWordsConversion.ConvertNumberToWords("12345678.45");

            string expectedWords = "twelve million three hundred and forty-five thousand six hundred and seventy-eight dollars and forty-five cents".ToUpper();

            Assert.Equal(expectedWords, numberInWords);
        }

        [Fact]
        public void Number_Zero()
        {
            var numberInWords = numberToWordsConversion.ConvertNumberToWords("0.00");

            string expectedWords = "zero dollars".ToUpper();

            Assert.Equal(expectedWords, numberInWords);
        }

        [Fact]
        public void Empty_Value()
        {
            var numberInWords = numberToWordsConversion.ConvertNumberToWords("");

            string expectedWords = "Unable to convert the value to number".ToUpper();

            Assert.Equal(expectedWords, numberInWords);
        }

        [Fact]
        public void Blank_Value()
        {
            var numberInWords = numberToWordsConversion.ConvertNumberToWords("  ");

            string expectedWords = "Unable to convert the value to number".ToUpper();

            Assert.Equal(expectedWords, numberInWords);
        }

        [Fact]
        public void Alphabet_Value()
        {
            var numberInWords = numberToWordsConversion.ConvertNumberToWords("abcd");

            string expectedWords = "Unable to convert the value to number".ToUpper();

            Assert.Equal(expectedWords, numberInWords);
        }

        [Fact]
        public void Number_Bigger_Than_100Billion()
        {
            var numberInWords = numberToWordsConversion.ConvertNumberToWords("120000000000.56");

            string expectedWords = "The number is too big".ToUpper();

            Assert.Equal(expectedWords, numberInWords);
        }

    }


}
