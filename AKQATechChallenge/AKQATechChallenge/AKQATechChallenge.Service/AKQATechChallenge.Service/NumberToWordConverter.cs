﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AKQATechChallenge.Service
{
    /// <summary>
    /// Accepts a number and converts to words and returns in upper case
    /// </summary>
    public class NumberToWordConverter : INumberToWordConverter
    {
        public string ConvertNumberToWords(string number)
        {
            string words;

            if (Double.TryParse(number, out double value))
            {
                // limit the number to 100 billion
                words = (value <= 100000000000) ? ConvertValueToWords(value) : "The number is too big";
            }
            else
                words = "Unable to convert the value to number";

            return words.ToUpper();
        }

        /// <summary>
        /// Rounds the number to 2 decimal places and converts the numeric and parts parts to words separately
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string ConvertValueToWords(double value)
        {
            string strWords = "";
            string decimals = "";
            string input = Math.Round(value, 2).ToString();

            if (input.Contains("."))
            {
                decimals = input.Substring(input.IndexOf(".") + 1);
                // remove decimal part from input
                input = input.Remove(input.IndexOf("."));
            }

            // Convert input into words. save it into strWords
            if (Int64.TryParse(input, out long numValue))
                strWords = NumberToWords(numValue) + " Dollars";
            else
                Console.WriteLine("String could not be parsed.");


            if (decimals.Length > 0)
            {
                if (Int32.TryParse(decimals, out int precisionValue))
                    // if there is any decimal part convert it to words and add it to strWords.
                    strWords += " and " + NumberToWords(precisionValue) + " Cents";
            }

            return strWords;
        }

        /// <summary>
        /// Convrets negative number, zero and positive numbers upto 100 billion to words
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string NumberToWords(long number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000000) > 0)
            {
                words += NumberToWords(number / 1000000000) + " billion ";
                number %= 1000000000;
            }

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }

    }
}
