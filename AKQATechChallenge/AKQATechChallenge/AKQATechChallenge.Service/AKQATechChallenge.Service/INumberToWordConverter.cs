﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AKQATechChallenge.Service
{
    public interface INumberToWordConverter
    {
        string ConvertNumberToWords(string number);
    }
}
